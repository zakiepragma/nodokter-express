import { Sequelize } from "sequelize";

const db = new Sequelize("nodokter", "root", "password", {
  host: "localhost",
  dialect: "mysql",
});

export default db;
